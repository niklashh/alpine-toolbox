# Alpine-toolbox

[![pipeline status](https://gitlab.com/niklashh/alpine-toolbox/badges/master/pipeline.svg)](https://gitlab.com/niklashh/alpine-toolbox/-/commits/master)

This container can be used for running alpine inside [toolbox](https://github.com/containers/toolbox).

```console
toolbox create --image registry.gitlab.com/niklashh/alpine-toolbox:latest
toolbox enter alpine-toolbox
```

## Building manually

To build the toolbox container

```console
./build.sh
```

To start the toolbox

```console
toolbox create --image localhost/alpine-toolbox
toolbox enter alpine-toolbox
```

To remove the toolbox

```console
toolbox rm -f alpine-toolbox
```

## Caveats

As alpine uses the _musl_ C library, some programs installed per-user
might not work, for example `cargo` and other Rust tools.

## License

MIT
